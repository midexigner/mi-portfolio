<?php
/*
Plugin Name: MI Portfolio Masnory
Plugin URI: https://midexigner.com
Description: Enables a portfolio Masnory.
Version: 0.1
Author: MI Team
Author URI: https://midexigner.com/
License: GPLv2

https://developer.wordpress.org/reference/functions/wp_get_attachment_caption/
https://www.bobz.co/how-to-get-attachment-image-caption-alt-or-description/
https://stackoverflow.com/questions/34658739/wordpress-get-attachment-image-caption
https://github.com/devinsays/team-post-type
https://gist.github.com/devinsays/
https://github.com/justintadlock?tab=repositories
https://github.com/justintadlock/get-the-image
https://github.com/justintadlock/custom-content-portfolio
https://github.com/justintadlock/my-snippets
https://gist.github.com/devinsays/df39e6e3dd5ee177fee30f7e7df548d6#file-post-type-metaboxes-php
https://wordpress.org/support/article/post-formats/
*/
// Get image ID from URL
function mid_portfolio_get_image_id($image_url) {
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
    //var_dump($attachment);
        return $attachment[0];
}

// Register admin scripts for custom fields
function mid_portfolio_load_wp_admin_style() {
    wp_enqueue_media();
wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
    // admin always last
    ;
    wp_enqueue_style( 'mid_portfolio_admin_css', plugin_dir_url(__FILE__) . 'css/mid_portfolio_admin.css' );
    wp_enqueue_script( 'mid_portfolio_admin_script', plugin_dir_url(__FILE__) . 'js/mid_portfolio_admin.js' );
}
add_action( 'admin_enqueue_scripts', 'mid_portfolio_load_wp_admin_style' );
add_action( 'init', 'prefix_portfolio_register_post_types' );
function prefix_portfolio_register_post_types() {
	/* Register the Portfolio Project post type. */
	register_post_type(
		'portfolio_project',
		array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 12,
			'menu_icon'           => 'dashicons-portfolio',
			'can_export'          => true,
			'delete_with_user'    => false,
			'hierarchical'        => false,
			'has_archive'         => 'portfolio',
			'query_var'           => 'portfolio_project',
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			
			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'       => 'portfolio',
				'with_front' => false,
				'pages'      => true,
				'feeds'      => true,
				'ep_mask'    => EP_PERMALINK,
			),
			/* What features the post type supports. */
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'revision',
				'thumbnail'
			),
			/* Labels used when displaying the posts. */
			'labels' => array(
				'name'               => __( 'Projects',                   'example-textdomain' ),
				'singular_name'      => __( 'Project',                    'example-textdomain' ),
				'menu_name'          => __( 'Portfolio',                  'example-textdomain' ),
				'name_admin_bar'     => __( 'Portfolio Project',          'example-textdomain' ),
				'add_new'            => __( 'Add New',                    'example-textdomain' ),
				'add_new_item'       => __( 'Add New Project',            'example-textdomain' ),
				'edit_item'          => __( 'Edit Project',               'example-textdomain' ),
				'new_item'           => __( 'New Project',                'example-textdomain' ),
				'view_item'          => __( 'View Project',               'example-textdomain' ),
				'search_items'       => __( 'Search Projects',            'example-textdomain' ),
				'not_found'          => __( 'No projects found',          'example-textdomain' ),
				'not_found_in_trash' => __( 'No projects found in trash', 'example-textdomain' ),
				'all_items'          => __( 'Projects',                   'example-textdomain' ),
			)
		)
	);
}

// Allow thumbnails to be used on portfolio post type
add_theme_support( 'post-thumbnails', array( 'portfolio_project' ) );

add_action( 'init', 'prefix_portfolio_register_meta' );
function prefix_portfolio_register_meta() {
	/* Register post meta (note: you might want to alter the callbacks). */
	register_meta( 'post', 'url',        'esc_url_raw', '__return_false' );
	register_meta( 'post', 'client',     'strip_tags',  '__return_false' );
	register_meta( 'post', 'location',   'strip_tags',  '__return_false' );
	register_meta( 'post', 'start_date', 'strip_tags',  '__return_false' );
	register_meta( 'post', 'end_date',   'strip_tags',  '__return_false' );
}

add_action( 'init', 'prefix_portfolio_register_taxonomies' );
function prefix_portfolio_register_taxonomies() {
	/* Register the Portfolio Category taxonomy. */
	register_taxonomy(
		'portfolio_category',
		array( 'portfolio_project' ),
		array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => 'portfolio_category',
			
			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'         => 'portfolio/category',
				'with_front'   => false,
				'hierarchical' => true,
				'ep_mask'      => EP_NONE
			),
			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Project Categories', 'example-textdomain' ),
				'singular_name'              => __( 'Project Category',   'example-textdomain' ),
				'menu_name'                  => __( 'Categories',         'example-textdomain' ),
				'name_admin_bar'             => __( 'Category',           'example-textdomain' ),
				'search_items'               => __( 'Search Categories',  'example-textdomain' ),
				'popular_items'              => __( 'Popular Categories', 'example-textdomain' ),
				'all_items'                  => __( 'All Categories',     'example-textdomain' ),
				'edit_item'                  => __( 'Edit Category',      'example-textdomain' ),
				'view_item'                  => __( 'View Category',      'example-textdomain' ),
				'update_item'                => __( 'Update Category',    'example-textdomain' ),
				'add_new_item'               => __( 'Add New Category',   'example-textdomain' ),
				'new_item_name'              => __( 'New Category Name',  'example-textdomain' ),
				'parent_item'                => __( 'Parent Category',    'example-textdomain' ),
				'parent_item_colon'          => __( 'Parent Category:',   'example-textdomain' ),
				'separate_items_with_commas' => null,
				'add_or_remove_items'        => null,
				'choose_from_most_used'      => null,
				'not_found'                  => null,
			)
		)
	);
	/* Register the Portfolio Tag taxonomy. */
	register_taxonomy(
		'portfolio_tag',
		array( 'portfolio_project' ),
		array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_admin_column' => true,
			'hierarchical'      => false,
			'query_var'         => 'portfolio_tag',
		
			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'         => 'portfolio/tag',
				'with_front'   => false,
				'hierarchical' => false,
				'ep_mask'      => EP_NONE
			),
			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Project Tags',                   'example-textdomain' ),
				'singular_name'              => __( 'Project Tag',                    'example-textdomain' ),
				'menu_name'                  => __( 'Tags',                           'example-textdomain' ),
				'name_admin_bar'             => __( 'Tag',                            'example-textdomain' ),
				'search_items'               => __( 'Search Tags',                    'example-textdomain' ),
				'popular_items'              => __( 'Popular Tags',                   'example-textdomain' ),
				'all_items'                  => __( 'All Tags',                       'example-textdomain' ),
				'edit_item'                  => __( 'Edit Tag',                       'example-textdomain' ),
				'view_item'                  => __( 'View Tag',                       'example-textdomain' ),
				'update_item'                => __( 'Update Tag',                     'example-textdomain' ),
				'add_new_item'               => __( 'Add New Tag',                    'example-textdomain' ),
				'new_item_name'              => __( 'New Tag Name',                   'example-textdomain' ),
				'separate_items_with_commas' => __( 'Separate tags with commas',      'example-textdomain' ),
				'add_or_remove_items'        => __( 'Add or remove tags',             'example-textdomain' ),
				'choose_from_most_used'      => __( 'Choose from the most used tags', 'example-textdomain' ),
				'not_found'                  => __( 'No tags found',                  'example-textdomain' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
			)
		)
	);
}

function add_theme_caps() {
    // gets the administrator role
    $admins = get_role( 'editor' );

    $admins->add_cap( 'edit_manage_portfolio' ); 
    $admins->add_cap( 'edit_manage_portfolios' ); 
    $admins->add_cap( 'edit_other_manage_portfolios' ); 
    $admins->add_cap( 'publish_manage_portfolios' ); 
    $admins->add_cap( 'delete_manage_portfolio' ); 
    $admins->add_cap( 'read_manage_portfolio' ); 
    $admins->add_cap( 'read_private_manage_portfolios' ); 
}
// add_action( 'admin_init', 'add_theme_caps', 10,2);
add_filter('category_template', 'filter_category_template');
function filter_category_template($template){
    /* Get current category */
    $category = get_queried_object();

    /* Create hierarchical list of desired templates */
    $templates = array (
      'category.php',
      'custom-category-template.php', 
      'category-{$category->slug}.php',
      'category-{$category->term_id}.php', 
      'index.php'
    ); 


    return locate_template($templates);
}
// add_filter('template_include', 'mcd_set_template',10);

//Redirect to a preferred template.
function mcd_set_template() {
    echo TEMPLATEPATH;
    $template_path = TEMPLATEPATH . '/templates/' . "portfolio.php";
    if(file_exists($template_path)){
        include($template_path);
        exit;
    }
}

function ccp_enter_title_here( $title ) {
    $post = get_current_screen();
    
    if  ( 'portfolio_project' == $post->post_type ) {
        $title = esc_html__( 'Enter Project Title', 'custom-content-portfolio' ) ;
   }

   return $title;
}
#Filter the "enter title here" text.
add_filter( 'enter_title_here', 'ccp_enter_title_here', 10, 2 );
# Filter the bulk and post updated messages.
 add_filter( 'bulk_post_updated_messages', 'ccp_bulk_post_updated_messages', 5, 2 );
add_filter( 'post_updated_messages',      'ccp_post_updated_messages',      5    );
function ccp_post_updated_messages( $messages ) {
	global $post, $post_ID;
	$project_type = 'portfolio_project';
	if ('portfolio_project' !== $post->post_type )
		return $messages;
	// Get permalink and preview URLs.
	$permalink   = get_permalink( $post_ID );
	$preview_url = get_preview_post_link( $post );
	// Translators: Scheduled project date format. See http://php.net/date
	$scheduled_date = date_i18n( __( 'M j, Y @ H:i', 'custom-content-portfolio' ), strtotime( $post->post_date ) );
	// Set up view links.
	$preview_link   = sprintf( ' <a target="_blank" href="%1$s">%2$s</a>', esc_url( $preview_url ), esc_html__( 'Preview project', 'custom-content-portfolio' ) );
	$scheduled_link = sprintf( ' <a target="_blank" href="%1$s">%2$s</a>', esc_url( $permalink ),   esc_html__( 'Preview project', 'custom-content-portfolio' ) );
	$view_link      = sprintf( ' <a href="%1$s">%2$s</a>',                 esc_url( $permalink ),   esc_html__( 'View project',    'custom-content-portfolio' ) );
	// Post updated messages.
	$messages[ $project_type ] = array(
		 1 => esc_html__( 'Project updated.', 'custom-content-portfolio' ) . $view_link,
		 4 => esc_html__( 'Project updated.', 'custom-content-portfolio' ),
		 // Translators: %s is the date and time of the revision.
		 5 => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Project restored to revision from %s.', 'custom-content-portfolio' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		 6 => esc_html__( 'Project published.', 'custom-content-portfolio' ) . $view_link,
		 7 => esc_html__( 'Project saved.', 'custom-content-portfolio' ),
		 8 => esc_html__( 'Project submitted.', 'custom-content-portfolio' ) . $preview_link,
		 9 => sprintf( esc_html__( 'Project scheduled for: %s.', 'custom-content-portfolio' ), "<strong>{$scheduled_date}</strong>" ) . $scheduled_link,
		10 => esc_html__( 'Project draft updated.', 'custom-content-portfolio' ) . $preview_link,
	);
	return $messages;
}

function ccp_bulk_post_updated_messages( $messages, $counts ) {
	$type = '';
	$messages['portfolio_project']['updated']   = _n( '%s project updated.',                             '%s projects updated.',                               $counts['updated'],   'custom-content-portfolio' );
	$messages['portfolio_project']['locked']    = _n( '%s project not updated, somebody is editing it.', '%s projects not updated, somebody is editing them.', $counts['locked'],    'custom-content-portfolio' );
	$messages['portfolio_project']['deleted']   = _n( '%s project permanently deleted.',                 '%s projects permanently deleted.',                   $counts['deleted'],   'custom-content-portfolio' );
	$messages['portfolio_project' ]['trashed']   = _n( '%s project moved to the Trash.',                  '%s projects moved to the trash.',                    $counts['trashed'],   'custom-content-portfolio' );
	$messages['portfolio_project']['untrashed'] = _n( '%s project restored from the Trash.',             '%s projects restored from the trash.',               $counts['untrashed'], 'custom-content-portfolio' );
	return $messages;
}


// Add the Meta Box
function mid_portfolio_add_custom_meta_box() {
    add_meta_box(
        'custom_meta_box', // $id
        'Shift8 Portfolio Fields', // $title
        'mid_portfolio_show_custom_meta_box', // $callback
        'portfolio_project', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'mid_portfolio_add_custom_meta_box');

// Field Array
$prefix = 'mid_portfolio_';
$custom_meta_fields = array(
    array(
        'label'=> 'Main Image',
        'desc'  => 'This is the main image that is shown in the grid and at the top of the single item page.',
        'id'    => $prefix.'image',
        'type'  => 'media'
    ),
    array(
        'label'=> 'Mobile Tile Image',
        'desc'  => 'This is the image that is shown in the portfolio list grid page on mobile only, specifically in portrait mode.',
        'id'    => $prefix.'mobileimage',
        'type'  => 'mobile'
    ),
    array(
        'label'=> 'Gallery Images',
        'desc'  => 'This is the gallery images on the single item page.',
        'id'    => $prefix.'gallery',
        'type'  => 'gallery'
    ),
);

// The Callback
function mid_portfolio_show_custom_meta_box($object) {
        global $custom_meta_fields, $post;
        // Use nonce for verification
        echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

        // Begin the field table and loop
        echo '<table class="form-table">';
        foreach ($custom_meta_fields as $field) {
                // get value of this field if it exists for this post
                $meta = get_post_meta($post->ID, $field['id'], true);
                // begin a table row with
                echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
                switch($field['type']) {
                        case 'media':
                        $close_button = null;
                        if ($meta) {
                                $close_button = '<span class="mid_portfolio_close"></span>';
                        }
                        echo '<input id="mid_portfolio_image" type="hidden" name="mid_portfolio_image" value="' . esc_attr($meta) . '" />
                        <div class="mid_portfolio_image_container">' . $close_button . '<img id="mid_portfolio_image_src" src="' . wp_get_attachment_thumb_url(mid_portfolio_get_image_id($meta)) . '"></div>
                        <input id="mid_portfolio_image_button" type="button" value="Add Image" />';
                        break;
                        case 'mobile':
                        $close_button = null;
                        if ($meta) {
                                $close_button = '<span class="mid_mobileportfolio_close"></span>';
                        }
                        echo '<input id="mid_portfolio_mobileimage" type="hidden" name="mid_portfolio_mobileimage" value="' . esc_attr($meta) . '" />
                        <div class="mid_portfolio_mobileimage_container">' . $close_button . '<img id="mid_portfolio_mobileimage_src" src="' . wp_get_attachment_thumb_url(mid_portfolio_get_image_id($meta)) . '"></div>
                        <input id="mid_portfolio_mobileimage_button" type="button" value="Add Image" />';
                        break;
                        case 'gallery':
                        $meta_html = null;
                        if ($meta) {
                                $meta_html .= '<ul class="mid_portfolio_gallery_list">';
                                $meta_array = explode(',', $meta);
                                foreach ($meta_array as $meta_gall_item) {
                                        $meta_html .= '<li><div class="mid_portfolio_gallery_container"><span class="mid_portfolio_gallery_close"><img id="' . esc_attr($meta_gall_item) . '" src="' . wp_get_attachment_thumb_url($meta_gall_item) . '"></span></div></li>';
                                }
                                $meta_html .= '</ul>';
                        }
                        echo '<input id="mid_portfolio_gallery" type="hidden" name="mid_portfolio_gallery" value="' . esc_attr($meta) . '" />
                        <span id="mid_portfolio_gallery_src">' . $meta_html . '</span>
                        <div class="mid_gallery_button_container"><input id="mid_portfolio_gallery_button" type="button" value="Add Gallery" /></div>';
                        break;
                } //end switch
                echo '</td></tr>';
        } // end foreach
        echo '</table>'; // end table
}

// Save the Data
function mid_portfolio_save_custom_meta($post_id) {
        global $custom_meta_fields;

        // Verify nonce
        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
                return $post_id;
        // Check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return $post_id;
        // Check permissions
        if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id))
                        return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
                return $post_id;
        }

        // Loop through meta fields
        foreach ($custom_meta_fields as $field) {
                $new_meta_value = esc_attr($_POST[$field['id']]);
		$meta_key = $field['id'];
                $meta_value = get_post_meta( $post_id, $meta_key, true );

                // If theres a new meta value and the existing meta value is empty
                if ( $new_meta_value && $meta_value == null ) {
                        add_post_meta( $post_id, $meta_key, $new_meta_value, true );
                // If theres a new meta value and the existing meta value is different
                } elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
                        update_post_meta( $post_id, $meta_key, $new_meta_value );
                } elseif ( $new_meta_value == null && $meta_value ) {
                        delete_post_meta( $post_id, $meta_key, $meta_value );
                }
        }
}

add_action('save_post', 'mid_portfolio_save_custom_meta');

